(() => {
  'use strict'

  const ctx = document.getElementById('myChart')

  const data = [
    { year: 2010, count: 10, color: '#BBDEFB' },
    { year: 2011, count: 20, color: '#90CAF9' },
    { year: 2012, count: 15, color: '#64B5F6' },
    { year: 2013, count: 25, color: '#42A5F5' },
    { year: 2014, count: 22, color: '#1E88E5' },
    { year: 2015, count: 30, color: '#1976D2' },
    { year: 2016, count: 28, color: '#1565C0' },
  ];
  
  const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: data.map(row => row.year),
      datasets: [{
        label: 'year',
        data: data.map(row => row.count),
        borderWidth: 1,
        backgroundColor: data.map(row => row.color),
      }]
    },
    options: {
      scales: {
        y: [{
          ticks: {
            beginAtZero: false
          }
        }]
      },
      legend: {
        display: false
      }
    }
  })
})()
