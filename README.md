# Modularized Bootstrap

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

A Bootstrap Experiment, with help of Nunjucks to modularized the HTML code.

> Pure HTML5 + Bootstrap5 CSS + Nunjucks + Custom SASS

## Experimental

This repository might change from time to time.

-- -- --

## Modular Nunjucks

Instead of a bunch of tags,
this code is based on building block of elements.

### The Ideas

This Nunjucks configuration is based on:
* [Karatasebu - Nunjucks with Gulp][nunjucks-with-gulp]

[nunjucks-with-gulp]: https://karatasebu.medium.com/nunjucks-with-gulp-be05c1ca7f4f

The gulpfile.js is provided int this repository.

### Install Gulp

For Nunjucks to be compiled, I utilize Gulp.

```
$ npm install --save-dev gulp gulp-nunjucks-render browser-sync merge-stream
```

### Running Gulp

```
$ gulp all-html
$ gulp watch
```

You can also run separately

```
$ gulp step-01
```


-- -- --

## Links

### Bootstrap 5

This repository:

* [Bootstrap 5 Step by Step Repository][tutorial-bootstrap]

### Comparation

Comparation with other guidance:

* [Bootstrap Open Color Step by Step Repository][tutorial-bootstrap-oc]

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md]

* [Materialize Step by Step Repository][tutorial-materialize]

* [Semantic UI Step by Step Repository][tutorial-semantic-ui]

[tutorial-jekyll]:      https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/
[tutorial-bootstrap]:   https://gitlab.com/epsi-rns/tutor-html-bootstrap/
[tutorial-bootstrap-oc]:https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc/
[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/
[tutorial-semantic-ui]: https://gitlab.com/epsi-rns/tutor-html-semantic-ui/

-- -- --

## Step 01

> Nunjucks Example

* Without Bootstrap

![screenshot][screenshot-00]

## Step 01

> Adding Bootstrap CSS

* Using CDN

* Using Local

![screenshot][screenshot-01]

-- -- --

## Step 02

> Using Bootstrap CSS: Navigation Bar

* From Simple to Full Featured

![screenshot][screenshot-02]

-- -- --

## Step 03

> Icon Choices

* Icons: FontAwesome, Boostrap Icon, Feathers Icon.

* Navbar and Sidebar

![screenshot][screenshot-03]

-- -- --

## Step 04

> Custom SASS

* Custom maximum width class

* Simple responsive layout using Bootstrap

![screenshot][screenshot-04]

-- -- --

## Step 05

> Color Choices

* Color using tailor made sass

* Open Color, Bootstrap5 Color, Google Material Color

* Double column demo, responsive gap using custom sass

* Single column demo, simple landing page

* Dark Mode

![screenshot][screenshot-05]

-- -- --

## Step 06

> HTML Box using Google Material Color

* Main Blog and Aside Widget

![screenshot][screenshot-06]

-- -- --

## Step 07

> Blog Post Example

* Post Header

* Post Navigation

* Landing Page

* Javascript Toggler

![screenshot][screenshot-07]

-- -- --

## Step 08

> Custom Javascript

* Hover and and Animation

* Numerator

* Progress Bar

![screenshot][screenshot-08]

-- -- --

## Step 09

> Form

* Login Form

* User Form

![screenshot][screenshot-09]

This form section deserve more coverage, more examples.

-- -- --

## Step 10

> Extra

* Datatables

* ChartJS

* Dashboard

![screenshot][screenshot-10]

Gather all together.

-- -- --

| Disclaimer | Use it at your own risk |
| ---------- | ----------------------- |

[screenshot-00]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap5/-/raw/main/step-00/html-bootstrap-preview.png
[screenshot-01]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap5/-/raw/main/step-01/html-bootstrap-preview.png
[screenshot-02]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap5/-/raw/main/step-02/html-bootstrap-preview.png
[screenshot-03]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap5/-/raw/main/step-03/html-bootstrap-preview.png
[screenshot-04]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap5/-/raw/main/step-04/html-bootstrap-preview.png
[screenshot-05]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap5/-/raw/main/step-05/html-bootstrap-preview.png
[screenshot-06]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap5/-/raw/main/step-06/html-bootstrap-preview.png
[screenshot-07]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap5/-/raw/main/step-07/html-bootstrap-preview.png
[screenshot-08]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap5/-/raw/main/step-08/html-bootstrap-preview.png
[screenshot-09]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap5/-/raw/main/step-09/html-bootstrap-preview.png
[screenshot-10]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap5/-/raw/main/step-10/html-bootstrap-preview.png