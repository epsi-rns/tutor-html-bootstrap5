const gulp = require("gulp")
const nunjucksRender = require("gulp-nunjucks-render")
const browserSync = require("browser-sync").create();
const merge = require('merge-stream');

const folders = [
  'step-00',
  'step-01',
  'step-02',
  'step-03',
  'step-04',
  'step-05',
  'step-06',
  'step-07',
  'step-08',
  'step-09',
  'step-10'
];

folders.map( (task) => {
  const folder = './'+task;
  gulp.task(task, () => {  
    return gulp.src(folder+'/views/*.njk')
      .pipe(nunjucksRender({
        path: [folder+'/views'],
      }))
      .pipe(gulp.dest(folder+'/'))
  });
});

gulp.task("all-html", () => {
  const tasks = folders.map( (folder) => {
    return gulp.src(folder+'/views/*.njk')
      .pipe(nunjucksRender({
        path: [folder+'/views']
      }))
      .pipe(gulp.dest(folder+'/'))
  });

  return merge(tasks);
});

gulp.task("watch", () => {
  browserSync.init({
    server: {
      baseDir: "./"
    },
    directory: true
  })

  gulp.watch('./**/*.html')
    .on("change", browserSync.reload);

  folders.map( (task) => {
    folder = './' + task
    gulp.watch(folder+'/views/**/*.njk',
      gulp.series(task));
  });
});
