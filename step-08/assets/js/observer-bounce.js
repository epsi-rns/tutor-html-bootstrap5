document.addEventListener(
  "DOMContentLoaded", function(event) { 

  let bounceObserver =
    new IntersectionObserver( (entries) => {
      entries.forEach((entry) => {
        const cl = entry.target.classList;
        if (entry.intersectionRatio > 0) {
          cl.add('animate__bounce');
        } else {
          cl.remove('animate__bounce');
        }
      });
  });

  const bouncesToObserve = document
    .querySelectorAll(".animate__observe__bounce");

  bouncesToObserve.forEach((element) => {
    bounceObserver.observe(element);
  });
});
