document.addEventListener(
  "DOMContentLoaded", function(event) { 

  const progressbars = document
    .querySelectorAll(".my__progressbar");
  const myBars       = document
    .querySelectorAll(".progress-bar");
  const containerBar = document
    .getElementById("container__bar");
  const total = containerBar.dataset.count;

  let containerBarObserver = 
    new IntersectionObserver( (entries) => {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > 0) {
          // reset on scroll
          myBars.forEach((element) => {
            element.classList.add('progress-bar');
          });

          progressbars.forEach((element) => {
            const count = element.dataset.count;
            const width = Math.round((count/total)*100);
            element.style.width = width + "%";

            element.setAttribute('aria-valuemin', 0);
            element.setAttribute('aria-valuemax', 100);
            element.setAttribute('aria-valuenow', width);

            const percText = element
              .getElementsByClassName("my__progressbar_perc")[0]
            percText.innerText = width + "%";
          });
        } else {
          myBars.forEach((element) => {
            element.classList.remove('progress-bar');
          });
        }
    });
  });

  containerBarObserver.observe(containerBar);
});